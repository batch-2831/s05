-- MySQL S5 Activity:

-- 1. Return the customerName of the customers who are from the Philippines

SELECT customerName from customers WHERE country ='Philippines';

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = 'La Rochelle Gifts';


-- 3. Return the product name and MSRP of the product named "The Titanic"

SELECT productName, MSRP FROM products WHERE productName ='The Titanic';


-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5. Return the names of customers who have no registered state

SELECT customerName FROM customers WHERE state IS NULL;

-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";


-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000

SELECT customerName, country, creditLimit FROM customers WHERE country != 'USA' AND creditLimit > 3000;


-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";


-- 9. Return the product lines whose text description mentions the phrase 'state of the art'

SELECT productLine FROM productlines WHERE textDescription LIKE "% state of the art%";



-- 10. Return the countries of customers without duplication

SELECT DISTINCT country FROM customers;


-- 11. Return the statuses of orders without duplication

SELECT DISTINCT status from orders;

-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada

SELECT customerName, country FROM customers WHERE country = "USA" OR country ="France" OR country = "Canada";


-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo

SELECT firstName, lastName, city FROM offices o
    JOIN employees e ON o.officeCode = e.officeCode
    WHERE city = "Tokyo";

-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"

SELECT customerName FROM customers c
    JOIN employees e ON c.salesRepEmployeeNumber = e.employeeNumber
    WHERE e.lastName = "Thompson" AND e.firstName = "Leslie";

-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"

SELECT productName, customerName FROM customers c 
    JOIN orders o ON o.customerNumber = c.customerNumber
    JOIN orderdetails od ON od.orderNumber = o.orderNumber
    JOIN products p ON p.productCode = od.productCode
    WHERE customerName = "Baane Mini Imports";


-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country

SELECT e.firstName, e.lastName, customerName, o.country FROM employees e 
JOIN offices o ON o.officeCode = e.officeCode
JOIN customers c ON c.salesRepEmployeeNumber = e.employeeNumber
WHERE o.country = c.country;



-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000

SELECT productName, quantityInStock from products
    WHERE productLine = "planes" AND quantityInStock < 1000;


-- 18. Show the customer's name with a phone number containing "+81".

SELECT customerName FROM customers WHERE phone LIKE "+81%";



